# dashReportAnalytics - Project

Small project to prove Swift knowledge.

This small project acts a dash board showing a few pieces of information that are useful when monitoring the uptime of a service. The ones on display here are the sites Status, Uptime, Users Connected, and Reports Submitted. The user can choose to refresh the app with a 3 second delay between request or the app will automatically refresh every 30 seconds. Each data field is updated upon a refresh. I will explain how these act here:

Status:
Status is simply wether the site is online or offline. The way this information is obtained is via Httpbin.org's base 64 API decoder. I send a it the word online or offline in a base 64 hashed string and it returns the decoded string as a raw UTF8 string. There is a 20% chance the site will be returned as offline.

Uptime:
Uptime is the amount of time the site has been online for. As there is not a site this amount of time is calculated when a refresh of the app happens. The app reaches out to Timezonedb.com's API for a UNIX timestamp and current date/time that is formatted as JSON; which is decoded using Swift's built in decoder. The timestamp is subtracted from the previous timestamp to get the amount of seconds the site has been online (this will reset if the site goes offline) and it also updates the time of last refresh to the current formatted date and time.

Users connected:
The amount of uses connected is a random number between 20 and 40. The app generates this number and then sends it to Httpbin.org via api request when the site then returns a cookie formatted as JSON. This JSON is decoded using Swift 4's JSON decoder. If the site is offline the number of users connected will display 0.

Reports Submitted:
At each refresh a number from 0-2 will be added to the amount of reports submitted. It is possible that no reports will be submitted upon a refresh. This number will only increase and will only reset after a restart of the app. The number is obtained the same way as the amount of Users connected.

Structs:
In total three Structs were used in this app. One for the data returned from Timezonedb.com and two for the cookie returned by Httpbin.org. This viewable in the project and are in their own file.
