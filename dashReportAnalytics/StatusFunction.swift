//
//  StatusFunction.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/11/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import Foundation

//This fucntion requests online/offline from httpbin.org by providing the words encoded in base64 and having httpbin decode them.
func getStatus(completionHandler: @escaping (String) -> ()) {
    
    //Set up the URL to request data from
    let base_url = "https://httpbin.org/base64/"
    //This array holds 4 "Online" and 1 "Offline" in base 64 to provide an 80% chance for the site to be online.
    let statusBase64 = ["T25saW5l", "T25saW5l", "T25saW5l", "T25saW5l", "T2ZmbGluZQ%3D%3D"]
    let urlString = base_url + statusBase64[Int(arc4random_uniform(5))]
    let url = URL(string: urlString)
    
    //Create the URL session and Data Task
    let session = URLSession.shared
    session.dataTask(with: url!) { (data, response, error) in
        
        //Make sure that no errors were returned and if they were print them to the console and exit
        guard error == nil else {
            print(error!)
            return
        }
        
        //Make sure data is returned, if none is print to the console and exit
        guard data != nil else {
            print("No Data Recived")
            return
        }
        
        //Return data back to the ViewController
        completionHandler(String(NSString(data: data!, encoding:String.Encoding.utf8.rawValue)!))
    }.resume() //start the data task
}
