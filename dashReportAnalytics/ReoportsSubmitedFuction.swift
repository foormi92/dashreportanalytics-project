//
//  ConnectedUsersFunction.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/11/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import Foundation

//This fuction sends a number between 0 and 2 to httpbin.org which is returned to via https request inside a cookie. The cookie is parsed and the data sent back to the ViewController.
func getReportsSubmitted(completionHandler: @escaping (Int) -> ()) {
    
    //set up the URL to request data from
    let base_url = "https://httpbin.org/cookies/set?reportsSubmitted="
    let reportsGenerated = arc4random_uniform(3).advanced(by: numReportsSubmitted)
    let urlString = base_url + String(reportsGenerated)
    let url = URL(string: urlString)
    
    //Create the URL session and Data Task
    let session = URLSession.shared
    session.dataTask(with: url!) { (data, response, error) in
        
        //Make sure that no errors were returned and if they were print them to the console and exit
        guard error == nil else {
            print(error!)
            return
        }
        
        //Make sure data is returned, if none is print to the console and exit
        if data != nil {
            let data = data!
            
            //Decode the cookie which is returned as JSON using the JSONDecoder
            do {
                let numReports = try JSONDecoder().decode(Cookie.self, from: data)
                //set data to the completeion handler to be returned to the ViewController
                completionHandler(Int(numReports.cookies.reportsSubmitted!)!)
           
            } catch {
                print (error)
            }
            
        } else {
            print("no data found")
            return
        }
    }.resume() //start the data task
}
