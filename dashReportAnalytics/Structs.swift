//
//  Structs.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/11/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import Foundation

//Time data structure
struct TimeData: Codable {
    let timestamp: Int
    let formatted: String
}

//Cookie data structure -> Containes site data
struct Cookie: Codable {
    let cookies: SiteData
}

//Site data structure
struct SiteData: Codable {
    let connectedUsers: String?
    let reportsSubmitted: String?
}
