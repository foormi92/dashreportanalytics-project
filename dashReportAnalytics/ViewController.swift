//
//  ViewController.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/9/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import UIKit

var siteStatus = "N/A"
var siteUptime = 0
var numConnectedUsers = "..."
var numReportsSubmitted: Int = 0
var previousTimeStamp:Int = 0

class ViewController: UIViewController {
    
    var refreshTimer = Timer()
    var lockBtnTimer = Timer()
    
    @IBOutlet weak var infoStatus: UILabel!
    @IBOutlet weak var infoUptime: UILabel!
    @IBOutlet weak var infoUsers: UILabel!
    @IBOutlet weak var infoReports: UILabel!
    @IBOutlet weak var infoLastUpdate: UILabel!
    @IBOutlet weak var refreshBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose of any resources that can be recreated.
    }
    
    //Handles what needs to happen when the refresh button is pressed
    @IBAction func btn(_ sender: Any) {
        refresh()
    }
    
    //Unclocks the refresh buttion and invalidates the timer linked to it
    @objc func unlock(){
        self.refreshBtn.isEnabled = true
        lockBtnTimer.invalidate()
    }
    
    //Refreshs data, locks the refresh button for 3 seconds and resets the 30 second timer to refresh the data
    @objc func refresh(){
        //Lock refresh button and start a 3 second timer to re-enable it
        self.refreshBtn.isEnabled = false
        lockBtnTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ViewController.unlock), userInfo: nil, repeats: true)
        
        //Get the server status
        getData()
        
        //Invalidate the currently running refresh timer and restart it
        refreshTimer.invalidate()
        refreshTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(ViewController.refresh), userInfo: nil, repeats: true)
        
        //Set the last time updated
        getCurrentTime(completionHandler: { (timeStamp, formatted) in
            DispatchQueue.main.async {
                self.infoLastUpdate.text = "Last Update: " + formatted
            }
        })
    }
    
    //Call API functions and populates lables with data recceived
    //There is a 20% chance that the site will be offline each refreash
    //If the site is offline Uptime will show last uptime and users connected will be 0
    //Uptime is displayed in seconds and is calculated using unix time fetched via API call
    //Connected users will be 0 (when offline) or between 20 and 40 (when online)
    //Every refresh a maximum of 2 reports can be submitted, these are added to the amount already submitted
    func getData(){
        
        //Get the status of the site (Online/Offline) and change the lable to reflect this
        getStatus(completionHandler: { (status) in
            siteStatus = status
            DispatchQueue.main.async{
                self.infoStatus.text = siteStatus
                if siteStatus == "Offline" {
                    previousTimeStamp = 0
                    siteUptime = 0
                    
                } else {
                    
                    //Get the most recent time and set uptime in seconds ad set lable
                    getCurrentTime(completionHandler: { (timeStamp, formatted) in
                        DispatchQueue.main.async {
                            if previousTimeStamp == 0 {
                                self.infoUptime.text = "0 Seconds - Since last Refresh"
                                previousTimeStamp = timeStamp
                                
                            } else {
                                siteUptime+=(timeStamp - previousTimeStamp)
                                previousTimeStamp = timeStamp
                                self.infoUptime.text = String(siteUptime) + " Seconds - Since last Refresh"
                                }
                            }
                        })
                    }
                }
            })
        
        //Get number of users connected and set the lable based on if the site is online
        getConnectedUsers(completionHandler: { (numUsers) in
            numConnectedUsers = numUsers
            DispatchQueue.main.async {
                if siteStatus == "Online" {
                    self.infoUsers.text = numConnectedUsers
                } else {
                    self.infoUsers.text = "0"
                }
            }
        })
        
        //Get Number of new Reports Submitted and set the lable
        getReportsSubmitted(completionHandler: { (numReports) in
            numReportsSubmitted = numReports
            DispatchQueue.main.async {
                self.infoReports.text = String(numReportsSubmitted)
            }
        })
    }
}
