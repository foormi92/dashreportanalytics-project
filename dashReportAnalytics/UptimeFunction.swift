//
//  UptimeFunction.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/11/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import Foundation

//This fuction reaches out to Timezonedb.com for the current time
func getCurrentTime(completionHandler: @escaping (Int, String) -> ()) {
    
    //set up the URL to request data from
    let urlString = "https://api.timezonedb.com/v2/get-time-zone?key=IESHK4P6AR3N&format=json&by=zone&zone=America/Chicago"
    let url = URL(string: urlString)
    
    //Create the URL session and Data Task
    let session = URLSession.shared
    session.dataTask(with: url!) { (data, response, error) in
        
        //Make sure that no errors were returned and if they were print them to the console and exit
        guard error == nil else {
            print(error!)
            return
        }
        
        //Make sure data is returned, if none is print to the console and exit
        if data != nil {
            let data = data!
            
            //Decode the cookie which is returned as JSON using the JSONDecoder, this is in a try block as the decoder and return an error
            do {
                let timeData = try JSONDecoder().decode(TimeData.self, from: data)
                //set data to the completeion handler to be returned to the ViewController
                completionHandler(timeData.timestamp, timeData.formatted)
                
            } catch {
                print (error)
            }
        }
            
        else {
            print("no data found")
            return
        }
    }.resume() //start the data task
}
