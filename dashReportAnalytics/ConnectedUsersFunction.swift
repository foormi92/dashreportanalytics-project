//
//  ConnectedUsersFunction.swift
//  dashReportAnalytics
//
//  Created by Michael Foor on 6/11/18.
//  Copyright © 2018 Michael Foor. All rights reserved.
//

import Foundation

//This fuction sends a number between 20 and 40 to httpbin.org which is returned to via https request inside a cookie. The cookie is parsed and the data sent back to the ViewController.
func getConnectedUsers(completionHandler: @escaping (String) -> ()) {
    
    //Set up URL to request data from
    let base_url = "https://httpbin.org/cookies/set?connectedUsers="
    let usersGenerated = Int(arc4random_uniform(21).advanced(by: 20))
    let urlString = base_url + String(usersGenerated)
    let url = URL(string: urlString)
    
    //Create the URL session and Data Task
    let session = URLSession.shared
    session.dataTask(with: url!) { (data, response, error) in
        
        //Make sure that no errors were returned and if they were print them to the console and exit
        guard error == nil else {
            print(error!)
            return
        }
        
        //Make sure data is returned, if none is print to the console and exit
        if data != nil {
            let data = data!
        
            //Decode the cookie which is returned as JSON using the JSONDecoder.
            do {
                let numUsers = try JSONDecoder().decode(Cookie.self, from: data)
                //set data to the completeion handler to be returned to the ViewController
                completionHandler(numUsers.cookies.connectedUsers!)
                
            } catch {
                print (error)
            }
        }
            
        else {
            print("no data found")
            return
        }
    }.resume() //start the data task
}
